const std = @import("std");
const Allocator = std.mem.Allocator;
const assert = std.debug.assert;
const print = std.debug.print;

pub fn main() !void {
    const allocator = std.heap.page_allocator;

    const input = try std.fs.cwd().readFileAlloc(allocator, "input_day01", std.math.maxInt(usize));
    defer allocator.free(input);

    var lines = std.mem.tokenize(input, "\n");

    var expenses = std.ArrayList(u16).init(allocator);
    defer expenses.deinit();
    while (lines.next()) |line| {
        try expenses.append(try std.fmt.parseInt(u16, line, 10));
    }

    part1(expenses);
    part2(expenses);
}

fn part1(expenses: std.ArrayList(u16)) void {
    for (expenses.items) |first_expense| {
        for (expenses.items) |second_expense| {
            if (first_expense + second_expense == 2020) {
                print("1: {}\n", .{first_expense});
                print("2: {}\n", .{second_expense});
                print("First final product: {}\n", .{@as(u32, first_expense) * second_expense});
                return;
            }
        }
    }
}

fn part2(expenses: std.ArrayList(u16)) void {
    for (expenses.items) |first_expense| {
        for (expenses.items) |second_expense| {
            for (expenses.items) |third_expense| {
                if (first_expense + second_expense + third_expense == 2020) {
                    print("1: {}\n", .{first_expense});
                    print("2: {}\n", .{second_expense});
                    print("3: {}\n", .{third_expense});
                    print("First final product: {}\n", .{@as(u32, first_expense) * second_expense * third_expense});
                    return;
                }
            }
        }
    }
}
